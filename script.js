const input = document.querySelector("#input");
const findBtn = document.querySelector("#findBtn");
let text = document.querySelector("#text");
let pronunciation = document.querySelector("#pronunciation");
//https://api.dictionaryapi.dev/api/v2/entries/en/bye

// fetch("https://api.dictionaryapi.dev/api/v2/entries/en/bye")
//   .then((response) => response.json())
//   .then((result) => {
//     let find = () => {
//       //   text.textContent = "";
//       for (let i = 0; i < result.length; i++) {
//         text.textContent += result[i].word;
//       }
//       console.log(result.length);
//     };
//     find();
//     console.log(result);
//   })
//   .catch((error) => console.log("error", error));

document.querySelector("#night").addEventListener("click", function () {
  document.querySelector("#moon").classList.toggle("d-none");
  document.querySelector("#sun").classList.toggle("d-none");
  document.querySelector("body").classList.toggle("bg-dark");
  input.classList.toggle("bg-dark");
  input.classList.toggle("text-light");
  text.classList.toggle("table-dark");
});

function find() {
  if (input.value.trim() != "") {
    fetch(`https://wordsapiv1.p.rapidapi.com/words/${input.value}`, {
      method: "GET",
      headers: {
        "x-rapidapi-host": "wordsapiv1.p.rapidapi.com",
        "x-rapidapi-key": "01be257534msh781515b212f94d6p1ebcacjsn9e7080983ffd",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        text.innerHTML = "";
        const tbody = document.createElement("tbody");
        text.appendChild(tbody);
        for (let i = 0; i < result.results.length; i++) {
          const tr = document.createElement("tr");
          tr.innerHTML = `<tr>\<td>${i + 1}</td> <td>(${
            result.results[i].partOfSpeech
          })</td><td>${result.results[i].definition}</td> </tr>  `;
          tbody.append(tr);
          pronunciation.textContent = `${result.word} - [${result.pronunciation.all}]`;
          console.log(result);
        }
      })
      .catch((err) => {
        text.textContent = "Not found !!!";
        console.error(err);
      });

    document.querySelector("#audio").classList.add("d-none");
    fetch(`https://api.dictionaryapi.dev/api/v2/entries/en/${input.value}`)
      .then((response) => response.json())
      .then((result) => {
        const src = `https:${result[0].phonetics[0].audio}`;
        const audio = new Audio(src);
        const player = document.querySelector("#player");
        player.src = src;
        document.querySelector("#audio").classList.remove("d-none");
        document.querySelector("#audio").onclick = function () {
          player.play();
          console.log(audio.src);
        };
      })
      .catch((error) => console.log("error", error));
  } else {
    text.textContent = "Please enter word";
  }
}
input.addEventListener("keyup", function (event) {
  if (event.key == "Enter") {
    find();
  }
});
